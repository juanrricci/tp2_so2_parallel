#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <omp.h>
#include "main.h"

void calculo_cant_muestras_gate()
{
    // Se calcula la cantidad de muestras por gate con la formula rango * validSamples / radio
    cant_muestras_gate_float = 0.5 * validSamples / 250;
    printf( "Cantidad de muestras por gate: %f\n", cant_muestras_gate_float );

    // Se obtiene el valor entero de muestras por gate
    cant_muestras_gate_int = ( int ) cant_muestras_gate_float;
    printf( "Valor entero de muestras por gate: %d\n", cant_muestras_gate_int );

    // Se obtiene el resto fraccional de muestras por gate
    resto_cant_muestras_gate_float = cant_muestras_gate_float - cant_muestras_gate_int;
    printf( "Resto de muestras por gate: %f\n", resto_cant_muestras_gate_float );

    // Triquiñuela para obtener la parte decimal del resto como un numero entero
    resto_cant_muestras_gate_k_float = resto_cant_muestras_gate_float * 1000 + 0.001;
    printf( "Resto de muestras por gate * 1000: %f\n", resto_cant_muestras_gate_k_float  );

    // Se extrae la parte entera, que es la que se usará para calcular la cantidad de muestras por gate.
    resto_cant_muestras_gate_k_int = ( int ) resto_cant_muestras_gate_k_float;
    printf( "Resto de muestras por gate en entero: %d\n", resto_cant_muestras_gate_k_int  );
    resto_cant_muestras_gate_actual_k_int = 0;
    muestra_extra_gate_int = 0;
    j = 0;
    memset( gates_canales.acum_tam_gates_previos, 0, sizeof( gates_canales.acum_tam_gates_previos ));

    // Se itera desde i = 0 hasta i < validSamples, saltando según la cantidad de muestras que le corresponde
    // a cada gate. Si la iteración es correcta, se tienen que producir exactamente 500 iteraciones
    // (correspondientes a los 500 gates de cada valor de validSamples)
    for( i = 0; i < validSamples; i += cant_muestras_gate_int + muestra_extra_gate_int )
    {
        // Se incrementa el resto de muestras del gate anterior con el del gate actual
        resto_cant_muestras_gate_actual_k_int += resto_cant_muestras_gate_k_int;

        // Si se completa una unidad de muestra extra, la variable muestra_extra_gate_int se pone en '1'.
        // También se resta 1000 al valor de resto_cant_muestras_gate_actual_k_int
        if( resto_cant_muestras_gate_actual_k_int >= 1000 )
        {
            muestra_extra_gate_int = 1;
            resto_cant_muestras_gate_actual_k_int -= 1000;

        } else
        {
            muestra_extra_gate_int = 0;
        }

        // En cada elemento de los arrays muestras_gate_canal_H y muestras_gate_canal_V se aloca la cantidad de bytes
        // necesarios para albergar las n (float) muestras correspondientes a ese gate.
        // También se guarda la cantidad de muestras que corresponden a cada gate en el array tamano_gate
        gates_canales.muestras_gates[ j ] = ( float * ) malloc( sizeof( float ) * ( cant_muestras_gate_int + muestra_extra_gate_int ));
        gates_canales.muestras_gates[ j + 500 ] = ( float * ) malloc( sizeof( float ) * ( cant_muestras_gate_int + muestra_extra_gate_int ));
        gates_canales.tamano_gate[ j ] = cant_muestras_gate_int + muestra_extra_gate_int;
        gates_canales.tamano_gate[ j + 500 ] = gates_canales.tamano_gate[ j ];
        if( j > 0 )
        {
            gates_canales.acum_tam_gates_previos[ j ] = gates_canales.acum_tam_gates_previos[ j - 1 ] + gates_canales.tamano_gate[ j - 1 ];
        }

        // Se imprime la cantidad de muestras que corresponden a cada gate. Se muestran las fracciones de muestra
        // que sobran entre un gate y otro. En el caso del gate 500 no sobra ninguna fracción de muestra.
        printf( "%d) Cant. muestras: %d. Acumuladas: %d. Fraccion restante: %f\n",
                j + 1,
                gates_canales.tamano_gate[ j ],
                gates_canales.acum_tam_gates_previos[ j ],
                ( float ) resto_cant_muestras_gate_actual_k_int / 1000 );

        j++;
    }

    // Se completan los acumulados de tamaño de gates previos hasta el final del array (desde 500 hasta 999)
    for( j = 500; j < 1000; j++ )
    {
        gates_canales.acum_tam_gates_previos[ j ] = gates_canales.acum_tam_gates_previos[ j - 1 ] + gates_canales.tamano_gate[ j - 1 ];
        printf( "%d) Cant. muestras: %d. Acumuladas: %d\n",
                j + 1,
                gates_canales.tamano_gate[ j ],
                gates_canales.acum_tam_gates_previos[ j ]);
    }
}

void operaciones_gate( int c )
{
    // Se copia el puntero que queda apuntando al inicio de la primer componente de la primera muestra a una variable
    // local que cada gate puede setear. Así, las componentes de las muestras propias de ese gate pueden ser
    // alcanzadas desde el mismo punto de referencia y no depender de los desplazamientos del puntero que generen
    // los gates previos.
    long int inicio_muestras = ftell( fp );
    FILE * fp_componentes = fopen( "pulsos.iq", "r" );
    fseek( fp_componentes, inicio_muestras, SEEK_SET );
    fseek( fp_componentes, gates_canales.acum_tam_gates_previos[ c ] * 4 * 2, SEEK_CUR );

    float muestra;
    float componente_I;
    float componente_Q;
    float acum_media_aritmetica = 0;
    float producto;
    float acum_autocorrelacion = 0;
    int k;
    char canal;

    // 1) 2) Cálculo del valor de cada muestra y alocación
    for( k = 0; k < gates_canales.tamano_gate[ c ]; k++ )
    {
        fread( &componente_I, 4, 1, fp_componentes ); // Lectura componente I
        fread( &componente_Q, 4, 1, fp_componentes ); // Lectura componente Q

        muestra = sqrt( powf( componente_I, 2 ) + powf( componente_Q, 2 )); // Cálculo del módulo de la muestra
        * ( gates_canales.muestras_gates[ c ] + k ) = muestra; // Se guarda la muestra en la memoria alocada

        acum_media_aritmetica += * ( gates_canales.muestras_gates[ c ] + k ); // Acumulación para cálculo de media aritmética

        if( k > 0 )
        {
            producto = * ( gates_canales.muestras_gates[ c ] + k - 1);
            producto *= * ( gates_canales.muestras_gates[ c ] + k ); // Producto de una muestra por la siguiente
            acum_autocorrelacion += producto; // Acumulación para cálculo de autocorrelación
        }
    }

    if( c < 500 ) canal = 'V';
    else canal = 'H';

    gates_canales.medias_aritmeticas[ c ] = acum_media_aritmetica / gates_canales.tamano_gate[ c ]; // Media aritmética
    printf( "%d) CANAL %c. Media aritmética = %f. Thread: %d\n", c + 1, canal, gates_canales.medias_aritmeticas[ c ], omp_get_thread_num());

    gates_canales.autocorrelaciones[ c ] = acum_autocorrelacion / gates_canales.tamano_gate[ c ]; // Autocorrelación
    printf( "%d) CANAL %c. Autocorrelación = %f. Thread: %d\n", c + 1, canal, gates_canales.autocorrelaciones[ c ], omp_get_thread_num());

    fclose( fp_componentes );
}

void operaciones_canales_V_H() // PARALELIZABLE
{
    int c;
    #pragma omp parallel for
    for( c = 0; c < 1000; c++ ) operaciones_gate( c );
    #pragma omp barrier
}

void escribir_binario_salida()
{
    fwrite( &validSamples, 2, 1, output_bin ); // Almacena el valor actual de validSamples (uint16_t: 2 bytes)

    for( j = 0; j < 1000; j++ )
    {
        // Almacena el valor de cantidad de muestras del gate (int: 4 bytes)
        fwrite( &gates_canales.tamano_gate[ j ], 4, 1, output_bin );
        // Almacena los valores de muestras del gate (float: 4 bytes)
        fwrite( gates_canales.muestras_gates[ j ], 4, gates_canales.tamano_gate[ j ], output_bin );
        // Almacena el valor de media aritmética del gate (float: 4 bytes)
        fwrite( &gates_canales.medias_aritmeticas[ j ], 4, 1, output_bin );
        // Almacena el valor de autocorrelación del gate (float: 4 bytes)
        fwrite( &gates_canales.autocorrelaciones[ j ], 4, 1, output_bin );
    }
}

void liberar_memoria_canales() // PARALELIZABLE
{
    int d;
    #pragma omp parallel for
    for( d = 0; d < 1000; d++ ) free( gates_canales.muestras_gates[ d ]);
    #pragma omp barrier

    printf( "Se liberó la memoria alocada de los gates de canal V y de canal H.\n" );
    printf( "**************************************************************************\n" );
}

void procesar_archivo_binario_entrada()
{
    // Se averigua la posicion actual del puntero en el archivo
    printf( "Posicion del puntero en el archivo antes de leer validSamples: %ld\n", ftell( fp ));

    // Este bucle se ejecuta mientras no se haya alcanzado el final del archivo
    // Posicion del puntero al inicio del valor de validSamples, más los dos bytes de tal valor, más los bytes de las
    // correspondientes muestras válidas
    while( 1 ) {
        // Se lee, en los dos bytes desde donde se encuentra el puntero, el valor de validSamples.
        // (Se pasa el puntero de la variable validSamples para almacenar el valor leído).
        fread( &validSamples, 2, 1, fp );
        // Se incrementa el contador de validSamples
        contador_validSamples++;
        printf("%d) Cantidad de validSamples : %d\n", contador_validSamples, validSamples);

        calculo_cant_muestras_gate();

        /***************************************************************************************************************
         * En este fragmento intermedio es donde hay que:
         * 1) Calcular el valor de cada muestra por Pitágoras.
         * 2) Alocarlo en la porción de memoria correspondiente.
         * 3) Calcular la media aritmética de cada gate.
         * 4) Calcular la autocorrelación de cada gate.
         * Después de esto, se libera la memoria alocada con el bucle for de abajo.
         **************************************************************************************************************/

        /***********        ***********
         * CANAL V *        * CANAL H *
         ***********        ***********/
        operaciones_canales_V_H();

        escribir_binario_salida();

        /**************************************************************************************************************/
        liberar_memoria_canales();

        // Se mueve el puntero a FILE tantos bytes como corresponden a validSamples
        fseek( fp, validSamples * 2 * 2 * 4, SEEK_CUR );
        // Se averigua la posicion actual del puntero en el archivo
        if( ftell( fp ) < size )
        {
            printf( "Posicion del puntero en el archivo antes de leer validSamples: %d\n", posicion_puntero_absoluta );
        }
        else break;
    }
}

int main ( )
{
    // Se establece la cantidad de threads que se usan en paralelo.
    //omp_set_num_threads( 8 );

    // Apertura del archivo y lectura de su tamaño
    fp = fopen( "pulsos.iq", "r" );
    if ( fp == NULL )
    {
        printf( "\nFile unable to open " );
    }
    else
    { 
        printf( "\nFile opened " );
    }
    fseek( fp, 0, 2 );    /* file pointer at the end of file */
    size = ftell( fp );   /* take a position of file pointer in size variable */
    printf( "The size of given file is : %d\n", size );
    rewind( fp );

    output_bin = fopen( "output.bin", "w" );

    procesar_archivo_binario_entrada();

    // Se corrobora que se alcanzó el final del archivo (se lo recorrió completo).
    printf( "Posicion del puntero en el archivo fuera del while : %ld\n", ftell( fp ));

    fclose( fp );
    fclose( output_bin );
}