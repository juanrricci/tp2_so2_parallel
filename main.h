//
// Created by john on 21/10/17.
//

#ifndef SOURCES_MAIN_H
#define SOURCES_MAIN_H

#endif //SOURCES_MAIN_H

FILE * fp; // Puntero a archivo binario de lectura
int size = 0; // Tamaño total de archivo (en bytes)

FILE * output_bin; // Puntero a archivo binario de salida

uint16_t validSamples; // Valor de validSamples
int posicion_puntero_absoluta = 0; // Posición actual del puntero que recorre el archivo (en bytes)
int posicion_puntero_offset = 0; // Desplazamiento del puntero que recorre el archivo (en bytes)
int contador_validSamples = 0; // Contador de cantidad de valores de validSamples recuperados del archivo

// Valor de cantidad de muestras por gate según el valor de validSamples obtenido.
// (Es un float, por lo tanto tiene parte entera y fraccional).
float cant_muestras_gate_float;
int cant_muestras_gate_int; // Parte entera del valor anterior.

// Parte fraccional de cant_muestras_gate_float (obtenida por sustracción de la parte entera)
float resto_cant_muestras_gate_float;
// Parte fraccional de cant_muestras_gate_float multiplicada por 1000. Se corrige el error de redondeo del float.
float resto_cant_muestras_gate_k_float;
int resto_cant_muestras_gate_k_int; // Parte entera del valor obtenido anteriormente.
// Esta variable, junto a la anterior, se usan para acumular las fracciones de muestras entre un gate y otro
// para incrementar el tamaño del gate que se está calculando actualmente, en caso de completar una unidad entera
// de muestra.
int resto_cant_muestras_gate_actual_k_int;
// Se pone en '1' si se completa un valor entero de muestra extra para el gate actual. Sino queda en '0'.
int muestra_extra_gate_int;

// Índice para iterar sobre los valores de cantidad de muestras de cada gate
int i;

// Estructura con dos arrays de punteros a float, correspondientes a canal V y H:
// Cada valor de cada array apunta a memoria alocada donde se encuentran las n muestras del gate correspondiente.
struct Gates_Canales
{
    int tamano_gate[ 1000 ];
    int acum_tam_gates_previos[ 1000 ];
    float * muestras_gates[ 1000 ];
    float medias_aritmeticas[ 1000 ];
    float autocorrelaciones[ 1000 ];
} gates_canales;

// Índices para apuntar a los elementos de los arrays de la estructura gates_canales
int j;

// Offset para mover el puntero en la memoria alocada e ir guardando los valores de las muestras calculadas.
//int k;

// Muestra calculada mediante Pitágoras a partir de sus componentes in-phase y quadrature
//float muestra;
//float componente_I;
//float componente_Q;
// Acumulado de muestras (para cálculo de media aritmética y autocorrelación)
//float muestra_acum;